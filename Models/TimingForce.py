from enum import Enum


class TimingForce(str, Enum):
    DAY = 'DAY',
    GTC = 'GTC',
    OPG = 'OPG',
    GTD = 'GTD',
    IOC = 'IOC',
    FOK = 'FOK'
